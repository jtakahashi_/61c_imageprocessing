#include <emmintrin.h>
#include <omp.h>
//#include <stdio.h>

#define KERNX 3 //this is the x-size of the kernel. It will always be odd.
#define KERNY 3 //this is the y-size of the kernel. It will always be odd.

// printf("(%d, %d, %d)\n", x, y, x+y*data_size_X);
// x+i>-1 && x+i<data_size_X && y+j>-1 && y+j<data_size_Y

int conv2D(float* in, float* out, int data_size_X, int data_size_Y, float* kernel) {
    // the x coordinate of the kernel's center
    const int kern_cent_X = (KERNX - 1)/2;
    // the y coordinate of the kernel's center
    const int kern_cent_Y = (KERNY - 1)/2;

    // Upper LH corner

    int x = 0, y = 0, i = 0, j = 0;
    
    #pragma omp parallel firstprivate(x, y, i, j)
    {
    #pragma omp single
    {
    x = 0; y = 0;
    for(int j = 0; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
        for(int i = 0; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
            out[0] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(i) + (j)*data_size_X];
        }
    }


    // Top row, y=0
    

    for (int x = kern_cent_Y; x < data_size_X - kern_cent_Y; x++) {
        for(int j = 0; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                out[x] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (j)*data_size_X];
            }
        }
    }
    
    // Upper RH corner
    x = data_size_X - kern_cent_X;
    for(int j = 0; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
        for(int i = -kern_cent_X; i <= 0; i++){ // kernel unflipped x coordinate
            out[x] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (j)*data_size_X];
        }
    }
    }

    
//middle
    //omp_set_num_threads(16);
    
    #pragma omp for nowait
    for (int y = kern_cent_X; y < data_size_Y - kern_cent_X; y++) {
        for (int x = kern_cent_Y; x < (data_size_X - kern_cent_Y - 1)/16*16; x+=16) {
            int x_ydata = x+y*data_size_X;
            for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
                int y_j = (y+j)*data_size_X+x;
                int kern_j = (kern_cent_Y-j)*KERNX;
                for(int i = -kern_cent_X; i <= kern_cent_X; i+=1){ // kernel unflipped x coordinate
                
                    __m128 in_vector = _mm_loadu_ps(in+ (i) + y_j); //vector of input
                    __m128 kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_j));
                    __m128 out_vector =  _mm_add_ps(_mm_loadu_ps(out+ x_ydata), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+x_ydata, out_vector);


                    in_vector = _mm_loadu_ps(in+ ((4)+i) + y_j); //vector of input
                    kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_j));
                    out_vector =  _mm_add_ps(_mm_loadu_ps(out+ (4)+x_ydata), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+(4)+x_ydata, out_vector);

                    in_vector = _mm_loadu_ps(in+ ((8)+i) + y_j); //vector of input
                    kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_j));
                    out_vector =  _mm_add_ps(_mm_loadu_ps(out+ (8)+x_ydata), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+(8)+x_ydata, out_vector);

                    in_vector = _mm_loadu_ps(in+ ((12)+i) + y_j); //vector of input
                    kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_j));
                    out_vector =  _mm_add_ps(_mm_loadu_ps(out+ (12)+x_ydata), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+(12)+x_ydata, out_vector);

                    //

                }
            }
        }
        
            //tail case
        for (int x = (data_size_X - kern_cent_Y - 1)/16*16+1; x < (data_size_X - kern_cent_Y); x++) {
            for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
                for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                    out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
                }
            }
        }   
    }

    #pragma omp single nowait
    {

    // Left
    x = 0;
    for (int y = kern_cent_X; y < data_size_Y - kern_cent_X; y++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = 0; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                out[y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
            }
        }
    }

    // Lower LH corner
    y = data_size_Y - kern_cent_Y;
    for(int j = -kern_cent_Y; j <= 0; j++){ // kernel unflipped y coordinate
        for(int i = 0; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
            out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
        }
    }
    
    }
    #pragma omp single nowait
    {

    // Bottom
    //#pragma omp parallel for firstprivate(x, j, i)
    y = data_size_Y - kern_cent_Y;
    for (int x = kern_cent_Y; x < data_size_X - kern_cent_Y; x++) {
        for(int j = -kern_cent_Y; j <= 0; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
            }
        }
    }

    // Lower RH corner
    x = data_size_X - kern_cent_X;
    for(int j = -kern_cent_Y; j <= 0; j++){ // kernel unflipped y coordinate
        for(int i = -kern_cent_X; i <= 0; i++){ // kernel unflipped x coordinate
            out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
        }
    }

    }
    #pragma omp single nowait
    {
    x = data_size_X - kern_cent_X;
    // Right
    for (int y = kern_cent_X; y < data_size_Y - kern_cent_X; y++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= 0; i++){ // kernel unflipped x coordinate
                out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
            }
        }
    }
    }
    } 
    
    return 1;
}
