#include <emmintrin.h>
#include <stdio.h>
#define KERNX 3 //this is the x-size of the kernel. It will always be odd.
#define KERNY 3 //this is the y-size of the kernel. It will always be odd.

// printf("(%d, %d, %d)\n", x, y, x+y*data_size_X);

int conv2D(float* in, float* out, int data_size_X, int data_size_Y, float* kernel) {
    // the x coordinate of the kernel's center
    int kern_cent_X = (KERNX - 1)/2;
    // the y coordinate of the kernel's center
    int kern_cent_Y = (KERNY - 1)/2;

    // Top
    int y = 0;
    for (int x = 0; x < data_size_X; x++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                if(x+i>-1 && x+i<data_size_X && y+j>-1) {
                    out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
                }
            }
        }
    }

    // Bottom
    y = data_size_Y - kern_cent_Y;
    for (int x = 0; x < data_size_X; x++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                if(x+i>-1 && x+i<data_size_X && y+j<data_size_Y) {
                    out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
                }
            }
        }
    }



    // Left
    int x = 0;
    for (int y = kern_cent_X; y < data_size_Y - kern_cent_X; y++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                if(x+i>-1 && y+j>-1 && y+j<data_size_Y){
                    out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
                }
            }
        }
    }

    // Right
    x = data_size_X - kern_cent_X;
    for (int y = kern_cent_X; y < data_size_Y - kern_cent_X; y++) {
        for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
            for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                if(x+i<data_size_X && y+j>-1 && y+j<data_size_Y){
                    out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
                }
            }
        }
    }


    for (int y = kern_cent_X; y < data_size_Y - kern_cent_X; y++) {
        for (int x = kern_cent_Y; x < (data_size_X - kern_cent_Y - 1)/16*16; x+=16) {
            for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
                for(int i = -kern_cent_X; i <= kern_cent_X; i+=1){ // kernel unflipped x coordinate

                
                    __m128 in_vector = _mm_loadu_ps(in+ (x+i) + (y+j)*data_size_X); //vector of input
                    __m128 kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_cent_Y-j)*KERNX);
                    __m128 out_vector =  _mm_add_ps(_mm_loadu_ps(out+ x+y*data_size_X), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+x+y*data_size_X, out_vector);


                    in_vector = _mm_loadu_ps(in+ ((x+4)+i) + (y+j)*data_size_X); //vector of input
                    kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_cent_Y-j)*KERNX);
                    out_vector =  _mm_add_ps(_mm_loadu_ps(out+ (x+4)+y*data_size_X), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+(x+4)+y*data_size_X, out_vector);

                    in_vector = _mm_loadu_ps(in+ ((x+8)+i) + (y+j)*data_size_X); //vector of input
                    kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_cent_Y-j)*KERNX);
                    out_vector =  _mm_add_ps(_mm_loadu_ps(out+ (x+8)+y*data_size_X), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+(x+8)+y*data_size_X, out_vector);

                    in_vector = _mm_loadu_ps(in+ ((x+12)+i) + (y+j)*data_size_X); //vector of input
                    kernel_vector = _mm_load1_ps(kernel+(kern_cent_X-i)+(kern_cent_Y-j)*KERNX);
                    out_vector =  _mm_add_ps(_mm_loadu_ps(out+ (x+12)+y*data_size_X), _mm_mul_ps(in_vector, kernel_vector));
                    _mm_storeu_ps(out+(x+12)+y*data_size_X, out_vector);


                }
            }
        }
        
            //tail case
        for (int x = (data_size_X - kern_cent_Y - 1)/16*16+1; x < (data_size_X - kern_cent_Y); x++) {
            for(int j = -kern_cent_Y; j <= kern_cent_Y; j++){ // kernel unflipped y coordinate
                for(int i = -kern_cent_X; i <= kern_cent_X; i++){ // kernel unflipped x coordinate
                    out[x+y*data_size_X] += kernel[(kern_cent_X-i)+(kern_cent_Y-j)*KERNX] * in[(x+i) + (y+j)*data_size_X];
                }
            }
        }
        
    }

    return 1;
}